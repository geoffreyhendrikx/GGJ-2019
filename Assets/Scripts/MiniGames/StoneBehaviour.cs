﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneBehaviour : MonoBehaviour
{
    /// <summary>
    /// The stone rigidbody.
    /// </summary>
    [SerializeField]
    private Rigidbody myRb;

    private void Start()
    {
        if (!myRb)
            myRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        myRb.MovePosition( transform.position  + new Vector3((-.3f), 0, 0));
    }
}

﻿using UnityEngine;

public class ObstacleRunMiniGame : MonoBehaviour
{
    [System.Serializable]
    public struct LevelChunk
    {
        [SerializeField]
        public GameObject Level;

        [SerializeField]
        public GameObject[] PossibleObstacles;
    }

    public LevelChunk[] LevelChunks;

    public Transform Parent;

    public float LevelMoveSpeed;
    public float ChunkOffset;

    private int currentSpawnedChunks;
    public int ChunksToWin;
    public float SpeedIncrementationPerSecond;

    private float previousXPos;

    private int currentChunk = 0;


    private void Start()
    {
        previousXPos = Parent.transform.position.x;

        //for (int i = 0; i < 4; i++)
        //{
        //    GameObject spawnedChunk = GetNextChunk().gameObject;
        //    spawnedChunk.transform.parent = Parent;
        //    spawnedChunk.transform.localPosition = new Vector3(ChunkOffset * i, 0, 0);
        //}
    }

    public void Update()
    {
        LevelMoveSpeed += Time.deltaTime * SpeedIncrementationPerSecond;

        Parent.transform.position += Vector3.right * LevelMoveSpeed * Time.deltaTime;

        if (Parent.transform.position.x >= (previousXPos + ChunkOffset))
        {
            previousXPos = Parent.transform.position.x;
            LevelChunk newChunk = GetNextChunk();
            if (currentSpawnedChunks < ChunksToWin)
                ChooseChunk(newChunk);

            newChunk.Level.transform.parent = Parent;
            newChunk.Level.transform.SetAsFirstSibling();
            newChunk.Level.transform.localPosition = Parent.GetChild(1).transform.localPosition - new Vector3(ChunkOffset, 0, 0);

            currentSpawnedChunks++;

            if (currentSpawnedChunks >= (ChunksToWin + 2))
            {
                PersistentData.Instance.ParcourMiniGameDone = true;
                Game.Instance.LoadSceneWithFadeOut("Master Night");
            }
        }
    }

    private void ChooseChunk(LevelChunk chunk)
    {
        for (int i = 0; i < chunk.PossibleObstacles.Length; i++)
            chunk.PossibleObstacles[i].SetActive(false);

        chunk.PossibleObstacles[Random.Range(0, chunk.PossibleObstacles.Length)].SetActive(true);
    }

    private LevelChunk GetNextChunk()
    {
        LevelChunk tempTargetChunk = LevelChunks[currentChunk];
        currentChunk++;

        if (currentChunk >= LevelChunks.Length)
            currentChunk = 0;

        return tempTargetChunk;
    }

}

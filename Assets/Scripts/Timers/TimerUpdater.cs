﻿using System.Collections;
namespace UnityEngine.Timers
{
    public class TimerUpdater : MonoBehaviour
    {
        public TimerManager Instance;

        public void Update()
        {
            if (Instance)
                Instance.Update();
        }

        /// <summary>
        /// Starts a coroutine.
        /// </summary>
        /// <param name="timerID">The ID of the timer</param>
        /// <param name="method">The coroutine method</param>
        public IEnumerator StartTimer(Timer timer, IEnumerator method)
        {
            yield return method;
            TimerManager.RemoveTimer(timer, true);
        }
    }
}

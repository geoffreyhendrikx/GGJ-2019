﻿using UnityEngine;

public class CutSceneAudio : MonoBehaviour
{
    public AudioClip[] Audio;

    public void PlayAudio(string name)
    {
        AudioClip clip = GetAudio(name);

        if (clip)
        {
            AudioSource source = new GameObject("Audio").AddComponent<AudioSource>();
            source.loop = false;
            source.playOnAwake = false;

            source.clip = clip;
            source.Play();
        }
    }

    private AudioClip GetAudio(string name)
    {
        AudioClip clip = null;

        for (int i = 0; i < Audio.Length; i++)
            if (Audio[i].name == name)
                clip = Audio[i];

        return clip;
    }
}

﻿
namespace UnityEngine.Timers
{
    public class Timer
    {
        //Delegates
        public OnTimerStart OnTimerStarted;
        public OnTimerTick OnTimerTicked;
        public OnTimerEnd OnTimerEnded;

        //The original duration of this timer.
        private float originalDuration = 0f;
        public float OriginalDuration => originalDuration;

        //The elapsed time of this timer.
        private float elapsed = 0f;
        public float Elapsed => elapsed;

        //Bool to determine if the timer should continue or not.
        private bool paused = false;
        public bool Paused => paused;

        //Bool to represent of this timer is recursive or not.
        public bool Recursive = false;

        //Bool to represent if the timer is a coroutine or not.
        public bool IsCoroutine
        {
            get;
            private set;
        }
        public Coroutine ConnectedCoroutine;

        //Bool to determine if this timer is done.
        public bool Done => (Elapsed == originalDuration);

        //if this is set to true, at the end of each tick the TimerManager will remove all timers with this marked true.
        public bool MarkForCleanUp = false;

        //name of the timer.
        public string Name = "New Timer";

        //Constructors.
        public Timer(float duration)
        {
            originalDuration = duration;
        }
        public Timer(float duration, bool recursive)
        {
            originalDuration = duration;
            Recursive = recursive;
        }
        public Timer(float duration, bool recursive, bool paused)
        {
            originalDuration = duration;
            Recursive = recursive;
            this.paused = paused;
        }
        public Timer(bool isCoroutine)
        {
            paused = true;
            originalDuration = 0;
            IsCoroutine = isCoroutine;
        }

        /// <summary>
        /// Ticks the timer and returns boolean value to represent if the timer has completed or not.
        /// </summary>
        /// <param name="interval">Interval to tick the timer with</param>
        public virtual bool Tick(float interval)
        {
            if (elapsed == 0 && OnTimerStarted != null)
                OnTimerStarted.Invoke();

            if (elapsed + interval >= originalDuration)
            {
                elapsed = originalDuration;

                if (OnTimerTicked != null)
                    OnTimerTicked.Invoke();

                return true;
            }
            else
            {
                elapsed += interval;

                if (OnTimerTicked != null)
                    OnTimerTicked.Invoke();

                return false;
            }
        }

        /// <summary>
        /// Sets a new duration for this timer, WARNING: will reset the timer.
        /// </summary>
        /// <param name="newDuration">The new duration of the timer.</param>
        public virtual void SetDuration(float newDuration)
        {
            originalDuration = newDuration;
            Reset();
        }

        /// <summary>
        /// Resets this timer.
        /// </summary>
        public virtual void Reset(bool stop = false)
        {
            if (stop)
                Stop();

            elapsed = 0;
        }

        /// <summary>
        /// Resumes the timer (If the timer was paused.)
        /// </summary>
        public virtual void Resume()
        {
            paused = false;
        }

        /// <summary>
        /// Stops the ticking of this timer.
        /// </summary>
        public virtual void Stop()
        {
            paused = true;
        }

        /// <summary>
        /// Returns the current time of the timer as string format.
        /// </summary>
        /// <returns></returns>
        public virtual string FormatAsString(TimerFormat format = TimerFormat.Simple)
        {
            switch (format)
            {
                case TimerFormat.Simple:
                    return string.Format("(Duration = {0} | Elapsed = {1})", originalDuration, System.Math.Round(Elapsed, 2));
                case TimerFormat.Percentage:
                    return string.Format("(Percentage = {0})", System.Math.Round((Elapsed / originalDuration), 2));
            }

            return "";
        }

        //Delegates
        public delegate void OnTimerStart();
        public delegate void OnTimerTick();
        public delegate void OnTimerFixedTick();
        public delegate void OnTimerEnd();

        //Enum to determine the format type.
        public enum TimerFormat
        {
            Simple,
            Percentage
        }
    }
}

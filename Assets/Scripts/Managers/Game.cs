﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Game : Singleton<Game>
{
    private CameraHandler cameraHandler;

    [SerializeField]
    private Animator cutsceneAnimator;

    public UnityEvent OnSceneLoaded
    {
        get;
        private set;
    }

    public Kunk MainKunk;
    public Vector3 KunkResumePosition;
    public Vector3 KunkResumeRotation;
    public Vector3 CameraResumePosition;

    public GameObject[] ResumeActiveObjects;
    public GameObject[] ResumeInActiveObjects;

    protected void Start()
    {
        if(PersistentData.Instance.ParcourMiniGameDone)
        {
            cutsceneAnimator.enabled = false;

            MainKunk.transform.position = KunkResumePosition;
            MainKunk.transform.eulerAngles = KunkResumeRotation;

            CameraHandler.Instance.transform.position = CameraResumePosition;

            for (int i = 0; i < ResumeActiveObjects.Length; i++)
                ResumeActiveObjects[i].gameObject.SetActive(true);

            for (int i = 0; i < ResumeInActiveObjects.Length; i++)
                ResumeInActiveObjects[i].gameObject.SetActive(false);
        }
    }

    protected override void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);

        base.Awake();
      //  DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Defines a new camera handler.
    /// </summary>
    /// <param name="cameraHandler">The new camera handler</param>
    public void DefineNewCameraHandler(CameraHandler cameraHandler)
    {
        this.cameraHandler = cameraHandler;
    }

    /// <summary>
    /// Switches to the previous scene.
    /// </summary>
    public void PreviousScene(bool immediateLoad = false)
    {
        if (immediateLoad || !cameraHandler)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        else
            cameraHandler.FadeOutCamera(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1));
    }

    /// <summary>
    /// Switches to the next scene.
    /// </summary>
    public void NextScene(bool immediateLoad = false)
    {
        //if (immediateLoad || !cameraHandler)
        //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadSceneWithFadeOut(string name)
    {
        CameraHandler.Instance.FadeOutCamera(() => { SceneManager.LoadScene(name); });
    }

    public void SetCutsceneState(bool state)
    {
        cutsceneAnimator.enabled = state;
    }

    /// <summary>
    /// Restarts the entire game.
    /// </summary>
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Restarts the level.
    /// </summary>
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Exits the game.
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }
}

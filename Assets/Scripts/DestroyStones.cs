﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyStones : MonoBehaviour
{
    private bool visible = false;

    private void OnBecameVisible()
    {
        visible = true;
    }

    private void OnBecameInvisible()
    {
        if (visible)
            Destroy(this.gameObject);
    }
}

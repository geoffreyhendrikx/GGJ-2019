﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Timers;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Rigidbody myRb;

    [SerializeField]
    private BoxCollider collider;

    public LayerMask GroundLayerMask;

    private bool IsGrounded = true;

    [SerializeField]
    private float jumpForce;

    private Timer duckTimer;

    [SerializeField]
    private Animator anim;

    [SerializeField]
    private AudioClip[] painClips;

    [SerializeField]
    private AudioSource thisAudioSource;

    [SerializeField]
    private int healthPoints = 3;
    private int defaultHealthPoints = 3;

    private bool isSliding = false;

    [SerializeField]
    private Image healthPointsImage;

    // Start is called before the first frame update
    private void Start()
    {
        defaultHealthPoints = healthPoints;

        InputManager.Instance.OnBButtonPressedDown += DoSlide;
        InputManager.Instance.OnBButtonPressedUp += StopSlide;

        InputManager.Instance.OnAButtonPressedDown += Jump;

        anim.SetFloat("WalkSpeed", 1);
    }

    private void DoSlide()
    {
        if (IsGrounded)
        {
            anim.SetBool("IsSliding", true);
            collider.size = new Vector3(0.75f, 0.25f, 0.75f);
            collider.center = new Vector3(0, 0.15f, 0);
            isSliding = true;
        };
    }

    private void StopSlide()
    {
        anim.SetBool("IsSliding", false);
        collider.size = new Vector3(0.75f, 2f, 0.75f);
        collider.center = new Vector3(0, 1, 0);
        isSliding = false;
    }

    private void Update()
    {
        if (Physics.Raycast(transform.position, -transform.up, 0.1f, GroundLayerMask))
        {
            anim.SetBool("IsJumping", false);
            IsGrounded = true;
        }
        else
        {
            IsGrounded = false;
            anim.SetBool("IsJumping", true);
        }
    }

    /// <summary>
    /// Jump
    /// </summary>
    private void Jump()
    {
        if (IsGrounded && !isSliding)
        {
            myRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            anim.SetBool("IsJumping", true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            healthPoints--;
            healthPointsImage.fillAmount = (float)healthPoints / defaultHealthPoints;

            if (healthPoints >= 0)
                thisAudioSource.PlayOneShot(painClips[Random.Range(0, painClips.Length)]);

            if (healthPoints <= 0)
            {
                InputManager.Instance.OnBButtonPressedDown -= DoSlide;
                InputManager.Instance.OnBButtonPressedUp -= StopSlide;
                InputManager.Instance.OnAButtonPressedDown -= Jump;

                Game.Instance.LoadSceneWithFadeOut(SceneManager.GetActiveScene().name);
            }
        }
    }
}

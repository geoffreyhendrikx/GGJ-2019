﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine.Timers
{
    public class TimerManager : ScriptableObject
    {
        //Singleton instance.
        private static TimerManager _instance;
        private static TimerManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<TimerManager>();

                if (_instance == null)
                    _instance = CreateInstance<TimerManager>();

                return _instance;
            }
            set
            {
                if (_instance != null)
                {
                    //Debug.LogError("TimerManager is already defined.");
                    return;
                }

                //Debug.Log("New TimerManager has been created.");

                _instance = value;

            }
        }

        //List containing all timers.
        private List<Timer> allTimers = new List<Timer>();
        public List<Timer> AllTimers => allTimers;

        //Timers to remove from the list.
        private List<Timer> timersToRemove = new List<Timer>();

        //Calls update for this instance.
        private TimerUpdater updateCaller;

        #region Timer Create functions.

        /// <summary>
        /// Creates a self destructing timer, only use this for calling a function after a duration
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to call when the timer has ended.</param>
        public static void AutoDestructTimer(float duration, Timer.OnTimerEnd onTimerEnd)
        {
            AutoDestructTimer(duration, "New Autodestruct Timer", onTimerEnd);
        }

        /// <summary>
        /// Creates a self destructing timer, only use this for calling a function after a duration
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to call when the timer has ended.</param>
        public static void AutoDestructTimer(float duration, string name, Timer.OnTimerStart onTimerStart, Timer.OnTimerTick onTimerTick, Timer.OnTimerEnd onTimerEnd)
        {
            Timer timer = new Timer(duration);
            timer.OnTimerStarted += onTimerStart;
            timer.OnTimerTicked += onTimerTick;
            timer.OnTimerEnded += onTimerEnd;
            timer.Name = name;

            timer.OnTimerEnded += () => { timer.MarkForCleanUp = true; };
            Instance.AllTimers.Add(timer);
        }

        /// <summary>
        /// Creates a self destructing timer, only use this for calling a function after a duration
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to call when the timer has ended.</param>
        public static void AutoDestructTimer(float duration, string Name, Timer.OnTimerEnd onTimerEnd)
        {
            Timer timer = new Timer(duration);
            timer.OnTimerEnded += onTimerEnd;
            timer.Name = Name;
            timer.OnTimerEnded += () => { timer.MarkForCleanUp = true; };
            Instance.AllTimers.Add(timer);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <returns>Created timer.</returns>
        public static Timer CreateTimer(float duration)
        {
            return CreateTimer(duration, string.Empty, null, null, null);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <returns>Created timer.</returns>
        public static Timer CreateTimer(float duration, string name)
        {
            return CreateTimer(duration, name, null, null, null);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to invoke when timer is completed.</param>
        /// <returns>Created timer.</returns>
        public static Timer CreateTimer(float duration, Timer.OnTimerEnd onTimerEnd)
        {
            return CreateTimer(duration, string.Empty, null, null, onTimerEnd);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerStart">Function to invoke when timer has started.</param>
        /// <param name="onTimerTick">Function to invoke when timer has ticked.</param>
        /// <param name="onTimerEnd">Function to invoke when timer is completed.</param>
        /// <returns></returns>
        public static Timer CreateTimer(float duration = 0f,
            string name = "",
            Timer.OnTimerStart onTimerStart = null,
            Timer.OnTimerTick onTimerTick = null,
            Timer.OnTimerEnd onTimerEnd = null)
        {
            Timer timer = new Timer(duration);

            if (onTimerStart != null)
                timer.OnTimerStarted += onTimerStart;

            if (onTimerTick != null)
                timer.OnTimerTicked += onTimerTick;

            if (onTimerEnd != null)
                timer.OnTimerEnded += onTimerEnd;

            if (!string.IsNullOrEmpty(name))
                timer.Name = name;

            Instance.allTimers.Add(timer);

            return timer;
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="method">The method that should be called.</param>
        /// <param name="coroutine">The coroutine the timer has.</param>
        /// <param name="name">Name of the timer.</param>
        /// <returns></returns>
        public static Timer CreateTimer(IEnumerator method,
            string name = "")
        {
            int newTimerID = Instance.allTimers.Count;

            Timer timer = new Timer(true);
            timer.ConnectedCoroutine = Instance.updateCaller.StartCoroutine(Instance.updateCaller.StartTimer(timer, method));

            if (!string.IsNullOrEmpty(name))
                timer.Name = name;

            Instance.allTimers.Add(timer);

            return timer;
        }
        #endregion

        /// <summary>
        /// Called every frame.
        /// </summary>
        public void Update()
        {
            if (Time.timeScale == 0) return;

            TickAllTimers();
        }

        /// <summary>
        /// Returns the amount of running timers.
        /// </summary>
        /// <returns></returns>
        public int RunnningTimers()
        {
            int amount = 0;

            for (int i = 0; i < Instance.AllTimers.Count; i++)
                if (!Instance.AllTimers[i].Done && !Instance.AllTimers[i].Paused)
                    amount++;

            return amount;
        }

        /// <summary>
        /// Returns the amount of paused timers.
        /// </summary>
        /// <returns></returns>
        public int PausedTimers()
        {
            int amount = 0;

            for (int i = 0; i < Instance.AllTimers.Count; i++)
                if (!Instance.AllTimers[i].Done && Instance.AllTimers[i].Paused)
                    amount++;

            return amount;
        }

        /// <summary>
        /// Returns the amount of stopped timers.
        /// </summary>
        public int StoppedTimers()
        {
            int amount = 0;

            for (int i = 0; i < Instance.AllTimers.Count; i++)
                if (Instance.AllTimers[i].Done)
                    amount++;

            return amount;
        }

        public static void ClearAllTimers()
        {
            // Let's first clean all the coroutines.
            for (int i = 0; i < Instance.AllTimers.Count; i++)
            {
                if (Instance.AllTimers[i].IsCoroutine)
                    Instance.updateCaller.StopCoroutine(Instance.AllTimers[i].ConnectedCoroutine);
            }

            Instance.AllTimers.Clear();
        }
        /// <summary>
        /// Ticks all timers if necesarry
        /// </summary>
        private void TickAllTimers()
        {
            int currentIndex = -1;

            try
            {
                for (int i = 0; i < allTimers.Count; i++)
                {
                    currentIndex = i;

                    if (!allTimers[i].Paused)
                    {
                        bool status = allTimers[i].Tick(Time.deltaTime);

                        if (i >= allTimers.Count || allTimers[i] == null)
                            continue;


                        if (status)
                        {
                            if (i >= allTimers.Count)
                                break;

                            if (allTimers[i] != null && (allTimers[i].MarkForCleanUp || !allTimers[i].Recursive))
                                timersToRemove.Add(allTimers[i]);

                            if (allTimers[i].OnTimerEnded != null)
                                allTimers[i].OnTimerEnded.Invoke();

                            if (i >= allTimers.Count || allTimers[i] == null)
                                continue;

                            if (i < allTimers.Count && allTimers[i].Recursive)
                                allTimers[i].Reset();
                        }
                    }
                }

                if (timersToRemove.Count == 0)
                    return;

                for (int i = 0; i < timersToRemove.Count; i++)
                {
                    if (timersToRemove[i].ConnectedCoroutine != null)
                        Instance.updateCaller.StopCoroutine(timersToRemove[i].ConnectedCoroutine);

                    AllTimers.Remove(timersToRemove[i]);
                }

                timersToRemove.Clear();
            }
            catch (Exception e)
            {
                allTimers.RemoveAt(currentIndex);

                Debug.Log("Exception thrown in timer.");
                Debug.LogException(e);

            }
        }

        /// <summary>
        /// Removes a timer instantly
        /// </summary>
        /// <param name="givenTimer"></param>
        public static void RemoveTimer(Timer givenTimer, bool force = true)
        {
            if (givenTimer == null)
                return;

            if (force)
            {
                if (givenTimer.ConnectedCoroutine != null)
                    Instance.updateCaller.StopCoroutine(givenTimer.ConnectedCoroutine);

                Instance.AllTimers.Remove(givenTimer);
            }
            else
            {
                givenTimer.Stop();
                givenTimer.MarkForCleanUp = true;
            }
        }

        /// <summary>
        /// Called when this object is enabled.
        /// </summary>
        public void OnEnable()
        {
            hideFlags = HideFlags.HideInHierarchy;

            CreateTimerUpdater();
        }

        public static void CreateTimerUpdater()
        {
            if (Instance.updateCaller == null)
            {
                GameObject newGo = new GameObject("#TimerManager updater");
                GameObject.DontDestroyOnLoad(newGo);
                Instance.updateCaller = newGo.AddComponent<TimerUpdater>();
                Instance.updateCaller.Instance = Instance;
                Instance.updateCaller.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
        }
    }

}

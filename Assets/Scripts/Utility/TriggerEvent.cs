﻿using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    public bool IsActive = true;
    public bool DisableOnActivation = true;

    public LayerMask TriggerMask;

    public OnTriggerEvent OnObjectEntered;
    public OnTriggerEvent OnObjectLeft;

    public void OnTriggerEnter(Collider coll)
    {
        if (!IsActive)
            return;

        if (coll.gameObject.layer.LayerIsInMask(TriggerMask))
        {
            OnObjectEntered.Invoke(coll.gameObject);

            if (DisableOnActivation)
                IsActive = false;
        }
    }

    public void OnTriggerExit(Collider coll)
    {
        if (!IsActive)
            return;

        if (coll.gameObject.layer.LayerIsInMask(TriggerMask))
            OnObjectLeft.Invoke(coll.gameObject);
    }    
}

[System.Serializable]
public class OnTriggerEvent : UnityEvent<GameObject>
{
    
}

﻿using UnityEngine;

public class PersistentData : Singleton<PersistentData>
{
    public bool ParcourMiniGameDone;

    public void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}

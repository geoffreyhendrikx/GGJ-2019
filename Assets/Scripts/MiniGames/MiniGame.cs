using UnityEngine;

public class MiniGame : MonoBehaviour
{
    public virtual void StopMiniGame(){}
    public virtual void StartMiniGame(){}

    public virtual void OnMiniGameLost(){}
    public virtual void OnMiniGameWon(){}
}
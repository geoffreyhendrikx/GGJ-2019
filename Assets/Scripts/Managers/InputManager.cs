﻿using UnityEngine;

public class InputManager : Singleton<InputManager>
{
#if UNITY_EDITOR
    [SerializeField]
    private bool enableInputDebugging = false;
#endif

    public delegate void OnAButtonPressedDownMethod();
    public delegate void OnBButtonPressedDownMethod();
    public delegate void OnDirectionalInputPressedMethod(Vector2Int value);

    public delegate void OnAButtonPressedMethod();
    public delegate void OnBButtonPressedMethod();

    public delegate void OnAButtonPressedUpMethod();
    public delegate void OnBButtonPressedUpMethod();

    public bool AButtonPressed
    {
        get;
        private set;
    }
    public bool BButtonPressed
    {
        get;
        private set;
    }
    public Vector2Int DirectionalInputPressed
    {
        get;
        private set;
    }

    public OnAButtonPressedDownMethod OnAButtonPressedDown;
    public OnBButtonPressedDownMethod OnBButtonPressedDown;
    public OnDirectionalInputPressedMethod OnDirectionalInputPressed;

    public OnAButtonPressedMethod OnAButtonPressed;
    public OnBButtonPressedMethod OnBButtonPressed;

    public OnAButtonPressedUpMethod OnAButtonPressedUp;
    public OnBButtonPressedUpMethod OnBButtonPressedUp;

    [SerializeField]
    private bool inputEnabled = true;
    public bool InputEnabled
    {
        get
        {
            return inputEnabled;
        }
        private set
        {
            inputEnabled = value;
        }
    }

    private Vector2Int cachedDirectionalInput;

    /// <summary>
    /// Takes care of all the input in the game.
    /// </summary>
    protected override void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);

        base.Awake();

       // DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Checks the input of the buttons.
    /// </summary>
    private void Update()
    {
        if (!InputEnabled)
            return;

        AButtonPressed = Input.GetButton("A");
        if (Input.GetButtonDown("A"))
        {
            OnAButtonPressedDown?.Invoke();

#if UNITY_EDITOR
            if (enableInputDebugging)
            Debug.Log("A button pressed down.");
#endif
        }

        if (AButtonPressed)
        {
            OnAButtonPressed?.Invoke();

#if UNITY_EDITOR
            if (enableInputDebugging)
                Debug.Log("A button pressed.");
#endif
        }

        if (Input.GetButtonUp("A"))
        {
            OnAButtonPressedUp?.Invoke();

#if UNITY_EDITOR
            if (enableInputDebugging)
                Debug.Log("A button released.");
#endif
        }

        BButtonPressed = Input.GetButton("B");
        if (Input.GetButtonDown("B"))
        {
            OnBButtonPressedDown?.Invoke();

#if UNITY_EDITOR
            if (enableInputDebugging)
                Debug.Log("B button pressed down.");
#endif
        }

        if (BButtonPressed)
        {
            OnBButtonPressed?.Invoke();

#if UNITY_EDITOR
            if (enableInputDebugging)
                Debug.Log("B button pressed.");
#endif
        }

        if (Input.GetButtonUp("B"))
        {
            OnBButtonPressedUp?.Invoke();

#if UNITY_EDITOR
            if (enableInputDebugging)
                Debug.Log("B button released.");
#endif
        }

        int tempHorizontalButtonValue = (int)Input.GetAxis("Horizontal");
        int tempVerticalButtonValue = (int)Input.GetAxis("Vertical");
        DirectionalInputPressed = new Vector2Int(tempHorizontalButtonValue, tempVerticalButtonValue);

        if (DirectionalInputPressed != Vector2Int.zero)
        {
            OnDirectionalInputPressed?.Invoke(DirectionalInputPressed);

#if UNITY_EDITOR
            if (enableInputDebugging)
                Debug.Log("Directional button pressed: " + DirectionalInputPressed);
#endif
        }

        cachedDirectionalInput = DirectionalInputPressed;
    }

    /// <summary>
    /// Enables the input.
    /// </summary>
    public void EnableInput()
    {
        InputEnabled = true;
    }

    /// <summary>
    /// Disables the input.
    /// </summary>
    public void DisableInput()
    {
        InputEnabled = false;
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CutSceneControl : MonoBehaviour
{
    private Vector3 cachedOffset;
    public Animator WolfAnimator;
    public GameObject IntertactableRoot;
    public GameObject RainGameObject;
    public GameObject TomIsEenFaggotRib;

    public Animator CutSceneAnimator;
    public Animator ThunderAnimator;
    public ParticleSystem CampFire;
    public GameObject KunkTorch;
    public Vector3 Rotation;

    public Kunk Kunk;

    [SerializeField]
    private Vector3 ribFireplacePosition, ribFireplaceRotation;


    [System.Serializable]
    public struct MinigameKunk
    {
        [SerializeField]
        public Vector3 CameraOffset;

        [SerializeField]
        public Transform KunkTarget;
    }

    [SerializeField]
    private MinigameKunk[] minigameKunks;


    public void ToggleRibs(int state)
    {
        TomIsEenFaggotRib.gameObject.SetActive(state == 1);
    }

    public void PlaceRibsNextToFire()
    {
        TomIsEenFaggotRib.transform.SetParent(null);
        TomIsEenFaggotRib.gameObject.transform.position = ribFireplacePosition;
        TomIsEenFaggotRib.gameObject.transform.eulerAngles = ribFireplaceRotation;
    }

    private void Start()
    {
        cachedOffset = CameraHandler.Instance.FollowOffset;
    }

    public void ToggleTorch(int state)
    {
        KunkTorch.gameObject.SetActive(state == 1);
    }

    public void DoWolfGrowl()
    {
        WolfAnimator.SetTrigger("Growl");
    }

    public void MarkAsDontDestroy()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void DisableRain()
    {
        RainGameObject.gameObject.SetActive(false);
    }

    public void DropKunkTorch()
    {
        KunkTorch.gameObject.SetActive(false);
    }

    public void ActivateCampFire()
    {
        CampFire.gameObject.SetActive(true);
    }

    public void DoBettyBeg()
    {
        WolfAnimator.SetTrigger("DoBeg");
    }


    public void DoFadeTransition(int state)
    {
        if (state == 1)
            CameraHandler.Instance.FadeInCamera();
        else
            CameraHandler.Instance.FadeOutCamera();
    }

    public void StartKunkWalk()
    {
        Kunk.Animator.SetFloat("WalkSpeed", 1);
    }

    public void SetKunkTrigger(string trigger)
    {
        Kunk.Animator.SetTrigger(trigger);
    }

    public void StopKunkWalk()
    {
        Kunk.Animator.SetFloat("WalkSpeed", 0);
    }

    public void DoThunderStruck()
    {
        ThunderAnimator.SetTrigger("Thunder");
    }

    public void SetAnimatorState(int state)
    {
        CutSceneAnimator.enabled = state == 1;
    }

    public GameObject TamingGame;
    public void StartTamingGame()
    {
        TamingGame.gameObject.SetActive(true);
        InputManager.Instance.enabled = true;
        InputManager.Instance.EnableInput();
    }

    public void StartMinigame(int minigameNo)
    {
        CameraHandler.Instance.ChangeCameraOffset(minigameKunks[minigameNo].CameraOffset);
        CameraHandler.Instance.ChangeFollowTarget(minigameKunks[minigameNo].KunkTarget);
    }

    public void StopMinigame()
    {
        CameraHandler.Instance.ChangeCameraOffset(cachedOffset);
        CameraHandler.Instance.ChangeFollowTarget(Kunk.transform);
    }
}

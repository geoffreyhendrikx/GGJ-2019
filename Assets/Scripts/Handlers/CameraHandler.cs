﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CameraHandler : Singleton<CameraHandler>
{
    [SerializeField]
    private Transform followTransform;

    [SerializeField]
    private Vector3 followOffset;
    public Vector3 FollowOffset => followOffset;

    [SerializeField, Tooltip("The higher the number, the slower the camera is.")]
    private float smoothDamp = 0;

    [SerializeField]
    private float fadeDuration = 1;

    [SerializeField]
    private bool fadeInCameraOnStart = true;

    [SerializeField]
    private Image blackScreen;

    [SerializeField]
    private bool freezeX, freezeY, freezeZ;

    public delegate void OnFadeCompletedMethod();
    public OnFadeCompletedMethod OnFadeCompleted;


    private void Start()
    {
        Game.Instance.DefineNewCameraHandler(this);

        if (fadeInCameraOnStart)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, 1);
            FadeInCamera(null);
        }
    }

    /// <summary>
    /// Smoothens the camera.
    /// </summary>
    private void Update()
    {
        Vector3 tempVelocity = Vector3.zero;

        if (followTransform)
        {
            Vector3 newPos = transform.position, originalPos = transform.position;

            newPos = Vector3.SmoothDamp(transform.position, followTransform.position + followOffset, ref tempVelocity, smoothDamp);

            if (freezeX)
                newPos.x = originalPos.x;

            if (freezeY)
                newPos.y = originalPos.y;

            if (freezeZ)
                newPos.z = originalPos.z;

            transform.position = newPos;
        }
    }

    /// <summary>
    /// Changes the target to follow.
    /// </summary>
    /// <param name="target">Following target</param>
    public void ChangeFollowTarget(Transform target)
    {
        followTransform = target;
    }

    /// <summary>
    /// Fades out the camera.
    /// </summary>
    /// <param name="duration">The duration of fading out.</param>
    public void FadeOutCamera(Action action = null)
    {
        StopAllCoroutines();
        StartCoroutine(FadeCameraTo(1, action));
    }

    /// <summary>
    /// Fades in the camera.
    /// </summary>
    /// <param name="duration">The duration of fading in.</param>
    public void FadeInCamera(Action action = null)
    {
        StopAllCoroutines();
        StartCoroutine(FadeCameraTo(0, action));
    }

    /// <summary>
    /// Fades the black screen to a specific value.
    /// </summary>
    /// <param name="toValue">Which alpha value?</param>
    private IEnumerator FadeCameraTo(int toValue, Action action)
    {
        float tempFromAlphaValue = blackScreen.color.a, tempValue = 0;

        while (tempValue < fadeDuration)
        {
            yield return new WaitForEndOfFrame();
            tempValue += Time.deltaTime / fadeDuration;

            Color tempNewColor = blackScreen.color;

            if (tempValue >= fadeDuration)
            {
                tempValue = 1;

                if (OnFadeCompleted != null)
                    OnFadeCompleted.Invoke();
            }

            tempNewColor.a = Mathf.Lerp(tempFromAlphaValue, toValue, tempValue);
            blackScreen.color = tempNewColor;
        }

        if (action != null)
            action.Invoke();
    }

    /// <summary>
    /// Freezes all camera axises.
    /// </summary>
    public void FreezeAllAxises()
    {
        freezeX = true;
        freezeY = true;
        freezeZ = true;
    }

    /// <summary>
    /// Unfreezes all camera axises.
    /// </summary>
    public void UnfreezeAllAxises()
    {
        freezeX = false;
        freezeY = false;
        freezeZ = false;
    }

    public void ChangeCameraOffset(Vector3 newOffset)
    {
        followOffset = newOffset;
    }
}

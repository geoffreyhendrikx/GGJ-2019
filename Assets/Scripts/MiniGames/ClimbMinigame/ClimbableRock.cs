﻿using UnityEngine;

public class ClimbableRock : MonoBehaviour
{
    public int ID
    {
        get;
        private set;
    }

    private Rigidbody thisRigidbody;
    private AudioSource thisAudioSource;

    [SerializeField]
    private AudioClip breakAudioClip;

    [SerializeField]
    private bool rockBroken;
    public bool RockBroken => rockBroken;

    public bool BrokenRockRevealed
    {
        get;
        private set;
    }


    private void Awake()
    {
        if (rockBroken)
        {
            thisRigidbody = gameObject.AddComponent<Rigidbody>();
            thisRigidbody.isKinematic = true;

            // Set-up the audio source for the breaking rock.
            thisAudioSource = gameObject.AddComponent<AudioSource>();
            thisAudioSource.spatialBlend = .5f;
        }
    }

    /// <summary>
    /// Setting up the rock with an ID.
    /// </summary>
    /// <param name="id">ID</param>
    public void SetupRock(int id)
    {
        ID = id;
    }

    /// <summary>
    /// Returns a response for the hanging player.
    /// </summary>
    public void Break()
    {
        if (rockBroken)
        {
            thisAudioSource.PlayOneShot(breakAudioClip);
            thisRigidbody.isKinematic = false;
            BrokenRockRevealed = true;
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = rockBroken ? new Color(1, 0, 1, 0.2f) : new Color(0, 1, 0, 0.2f);
        Gizmos.DrawCube(transform.position, transform.lossyScale + Vector3.one * 0.01f);
    }
#endif
}

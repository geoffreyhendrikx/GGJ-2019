﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    [SerializeField]
    private float fadeOutTransitionDuration = 3;

    [SerializeField]
    private Image[] menuImages;


    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2);

        StartGame();
    }

    /// <summary>
    /// Restarting the game.
    /// </summary>
    public void StartGame()
    {
        StartCoroutine(FadeOutMenu());
    }

    private IEnumerator FadeOutMenu()
    {
        float tempValue = 0;
        Color[] fromColors = new Color[menuImages.Length], toColors = new Color[menuImages.Length];

        for (int i = 0; i < menuImages.Length; i++)
        {
            fromColors[i] = menuImages[i].color;
            toColors[i] = new Color(menuImages[i].color.r, menuImages[i].color.g, menuImages[i].color.b, 0);
        }

        while (tempValue < 1)
        {
            tempValue += Time.deltaTime / fadeOutTransitionDuration;

            for (int i = 0; i < menuImages.Length; i++)
                menuImages[i].color = Color.Lerp(fromColors[i], toColors[i], tempValue);

            yield return new WaitForEndOfFrame();

            if (tempValue >= 1)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    /// <summary>
    /// Quits the game
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }

}

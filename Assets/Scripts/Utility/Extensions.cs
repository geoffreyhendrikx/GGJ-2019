﻿using UnityEngine;
public static class Extensions
{
    public static bool LayerIsInMask(this int layer, LayerMask layerMask)
    {
        int collLayerMask = (1 << layer);

        if ((layerMask.value & collLayerMask) > 0)
            return true;
        else
            return false;
    }
}

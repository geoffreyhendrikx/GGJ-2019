﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngryWolf : MonoBehaviour
{
    public AudioSource wolfgrowl;

    private void OnCollisionEnter(Collision collision)
    {
        wolfgrowl.Play();
    }
}

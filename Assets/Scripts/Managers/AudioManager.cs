﻿using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField]
    private AudioSource musicSource;
    private AudioClip[] musicClips;

    [SerializeField]
    private uint defaultMusicID;

    [SerializeField]
    private AudioSource[] sfxSources;

    [SerializeField]
    private AudioClip[] sfxClips;


    /// <summary>
    /// Plays a certain music clip.
    /// </summary>
    /// <param name="musicClipID">Music clip ID</param>
    public void PlayMusic(uint musicClipID)
    {
        if (musicClipID >= musicClips.Length)
        {
            Debug.LogError("Can't find music clip ID " + musicClipID);
            return;
        }

        musicSource.clip = musicClips[musicClipID];
        musicSource.Play();
    }

    /// <summary>
    /// Pauses the music.
    /// </summary>
    public void PauseMusic()
    {
        musicSource.Pause();
    }

    /// <summary>
    /// Stops the music.
    /// </summary>
    public void StopMusic()
    {
        musicSource.Stop();
    }

    /// <summary>
    /// Plays a certain sound effect.
    /// </summary>
    /// <param name="sfxID">Sound effect ID</param>
    /// <param name="audioSourceID">Audio source ID</param>
    public void PlaySFX(int sfxID, int audioSourceID = 0)
    {
        sfxSources[audioSourceID].PlayOneShot(sfxClips[sfxID]);
    }

    /// <summary>
    /// Sets the global volume.
    /// </summary>
    /// <param name="value">The value of the volume</param>
    public void SetGlobalVolume(float value)
    {
        AudioListener.volume = value;
    }
}

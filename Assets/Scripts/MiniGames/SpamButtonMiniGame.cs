﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Timers;
using UnityEngine.UI;

public class SpamButtonMiniGame : MiniGame
{
    public GameObject Player;
    public GameObject BadGuy;
    public Animator WeaponAnimator;
    public Image[] InputButtons;

    private InputType currentInputType;

    public float Incrementation;
    public float BadGuyIncremenation;
    private bool hitWasDone;

    public float MinXCoordinateForLose;
    public float MaxCoordinateForWin;
    private bool miniGameStarted;

    private void Start()
    {
        CameraHandler.Instance.OnFadeCompleted += StartMiniGame;
    }

    public override void StartMiniGame()
    {
        InputManager.Instance.OnAButtonPressed += () => { Hit(InputType.AButton); };
        InputManager.Instance.OnBButtonPressed += () => { Hit(InputType.BButton); };

        InputManager.Instance.OnDirectionalInputPressed += (Vector2Int input) =>
        {
            InputType type = InputType.AButton;

            if (input.x == -1 && input.y == 0)
                type = InputType.DpadLeft;

            if (input.x == 1 && input.y == 0)
                type = InputType.DpadRight;

            if (input.x == 0 && input.y == 1)
                type = InputType.DpadUp;

            if (input.x == 0 && input.y == -1)
                type = InputType.DpadDown;

            Hit(type);
        };

        miniGameStarted = true;
        AssignNewInputType();
    }

    private void AssignNewInputType()
    {
        InputType last = currentInputType;
        currentInputType = (InputType)(int)Random.Range((int)0, (int)6);

        if(currentInputType == last)
        {
            AssignNewInputType();
            return;
        }

        for (int i = 0; i < 6; i++)
            InputButtons[i].gameObject.SetActive(false);

        InputButtons[(int)currentInputType].gameObject.SetActive(true);
    }

    public void Update()
    {
        if (!miniGameStarted)
            return;

        if (hitWasDone)
        {
            hitWasDone = false;
            return;
        }

        Vector3 incrementation = Vector3.left * Time.deltaTime * BadGuyIncremenation;
        Player.transform.position += incrementation;
        BadGuy.transform.position += incrementation;

        if (BadGuy.transform.position.x > MaxCoordinateForWin)
            OnMiniGameWon();

        if (BadGuy.transform.position.x < MinXCoordinateForLose)
            OnMiniGameLost();
    }

    public override void OnMiniGameLost()
    {
        Debug.Log("Lost spam minigame");
        miniGameStarted = false;
        CameraHandler.Instance.FadeOutCamera();

        CameraHandler.Instance.OnFadeCompleted -= StartMiniGame;
        CameraHandler.Instance.OnFadeCompleted += () => { SceneManager.LoadScene(SceneManager.GetActiveScene().name); };
    }

    public override void OnMiniGameWon()
    {
        Debug.Log("Won spam minigame");
        miniGameStarted = false;

        CameraHandler.Instance.FadeOutCamera();

        CameraHandler.Instance.OnFadeCompleted -= StartMiniGame;
        CameraHandler.Instance.OnFadeCompleted += () => { SceneManager.LoadScene(SceneManager.GetActiveScene().name); };
    }

    public void Hit(InputType input)
    {
        if (!miniGameStarted)
            return;

        if(input != currentInputType)
        {
            TimerManager.AutoDestructTimer(1, () => {  });
            return;
        }

        AssignNewInputType();


        Vector3 incrementation = Vector3.right * Time.deltaTime * Incrementation;
        WeaponAnimator.SetTrigger("Hit");

        Player.transform.position += incrementation;
        BadGuy.transform.position += incrementation;
        hitWasDone = true;
    }
}
    public enum InputType
{
    DpadUp,
    DpadDown,
    DpadRight,
    DpadLeft,
    AButton,
    BButton
}
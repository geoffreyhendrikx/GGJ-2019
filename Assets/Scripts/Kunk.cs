﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunk : MonoBehaviour
{
    public float MovementSpeed;
    public float RightOrientation, LeftOrientation;
    public Animator Animator;
    public Rigidbody RigidBody;
    private float lastXInput;

    private void Start()
    {
        InputManager.Instance.OnDirectionalInputPressed += OnDirectionalInput;
    }

    public void Update()
    {
        Animator.SetFloat("WalkSpeed", Mathf.Abs(InputManager.Instance.DirectionalInputPressed.x));
    }

    public void OnDirectionalInput(Vector2Int input)
    {
        if (!enabled)
            return;

        if (input.x >= 0.1f)
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, RightOrientation, transform.eulerAngles.z);

        if (input.x <= 0.1f)
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, LeftOrientation, transform.eulerAngles.z);

        RigidBody.MovePosition(transform.position + (Vector3.right * input.x * MovementSpeed * Time.fixedDeltaTime));
    }
}

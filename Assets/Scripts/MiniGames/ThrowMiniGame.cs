﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ThrowMiniGame : MiniGame
{
    [Header("Arc Settings")]
    public GameObject ArcPointerGo;
    public Vector2 ArcPointerMinMaxX;
    public float ArcPointerSpeed;
    public int ArcPointCount;
    public float ArcHeight;
    public AnimationCurve ArcCurve;
    public LineRenderer ArcLineRenderer;
    private bool isDetermeningArc;
    private bool pingPong = false;
    public Animator Animator;
    public Image currentFoodLeftImage;

    [Header("Throw settings")]
    public GameObject FoodPrefab;
    private GameObject currentThrownFood;
    public int CurrentFoodLeft;
    private bool canThrow = true;

    [Header("Wolf settings")]
    public GameObject Wolf;
    public Animator WolfAnimator;
    public float MinDistanceForWolf;
    public float WolfTravelTime;
    public float MinWolfDistanceForWin = 10f;

    [Header("Events")]
    public UnityEvent OnWolfBaited;
    public UnityEvent OnWolfBaitFailed;

    public UnityEvent MiniGameStarted;
    public UnityEvent MiniGameWon;
    public UnityEvent MiniGameLost;

    private void Start()
    {
        StartMiniGame();
        //CameraHandler.Instance.OnFadeCompleted += StartMiniGame;
    }

    public void Update()
    {
        if (isDetermeningArc)
        {
            float incrementation = (pingPong ? 1 : -1) * ArcPointerSpeed;
            Vector3 newPosition = ArcPointerGo.transform.position += Vector3.right * incrementation * Time.deltaTime;
            newPosition.x = Mathf.Clamp(newPosition.x, transform.position.x + ArcPointerMinMaxX.x, transform.position.x + ArcPointerMinMaxX.y);

            if (newPosition.x == transform.position.x + ArcPointerMinMaxX.x || newPosition.x == transform.position.x + ArcPointerMinMaxX.y)
                pingPong = !pingPong;

            ArcPointerGo.transform.position = newPosition;

            Vector3[] throwArc = GetThrowArc(transform.position, ArcPointerGo.transform.position);
            UpdateLineRenderer(throwArc);
        }
    }

    public override void StartMiniGame()
    {
        MiniGameStarted.Invoke();
        gameObject.SetActive(true);
        isDetermeningArc = true;
        canThrow = true;
        InputManager.Instance.OnAButtonPressedDown += () =>
        {
            if (canThrow)
                StartThrow();
        };
    }

    public override void OnMiniGameLost()
    {
        Debug.Log("Wolf bait minigame lost");
        CameraHandler.Instance.FadeOutCamera();
        CameraHandler.Instance.OnFadeCompleted += () => { SceneManager.LoadScene(SceneManager.GetActiveScene().name); };
        MiniGameLost.Invoke();
    }

    public override void OnMiniGameWon()
    {
        MiniGameWon.Invoke();
    }

    public void StartThrow()
    {
        Animator.SetTrigger("Throw");
        canThrow = false;
        CurrentFoodLeft--;

        currentFoodLeftImage.fillAmount = (float)CurrentFoodLeft / (float)3; 
        Debug.Log("Food left: " + CurrentFoodLeft);

        currentThrownFood = Instantiate(FoodPrefab);
        currentThrownFood.transform.localScale = Vector3.one;
        currentThrownFood.transform.position = transform.position;

        isDetermeningArc = false;
        StartCoroutine(ThrowFood());
        SetLineRendererState(false);
    }

    private IEnumerator ThrowFood()
    {
        Vector3[] positions = GetThrowArc(transform.position, ArcPointerGo.transform.position);

        for (int i = 0; i < positions.Length; i++)
        {
            currentThrownFood.transform.position = positions[i];
            yield return null;
        }

        if (CurrentFoodLeft > 0)
            isDetermeningArc = true;

        DetermineThrowResult();
    }

    public void DetermineThrowResult()
    {
        float distance = (Wolf.transform.position - currentThrownFood.transform.position).magnitude;
        bool successfullThrow = (distance < MinDistanceForWolf);

        if (successfullThrow)
        {
            ArcPointerSpeed *= 2;

            OnWolfBaited.Invoke();
            StartCoroutine(MoveGameObject(Wolf,
            new Vector3(currentThrownFood.transform.position.x, Wolf.transform.position.y, Wolf.transform.position.z), WolfTravelTime, CheckForWin));
        }
        else if (!successfullThrow && CurrentFoodLeft > 0)
        {
            canThrow = true;
            currentThrownFood.SetActive(false);
        }
        else if (CurrentFoodLeft == 0)
        {
            OnMiniGameLost();
            OnWolfBaitFailed.Invoke();
        }

    }

    private void CheckForWin()
    {
        float dist = (transform.position - Wolf.transform.position).magnitude;
        currentThrownFood.SetActive(false);

        WolfAnimator.SetTrigger("Eat");

        Debug.Log(dist);
        if (dist < MinWolfDistanceForWin)
            OnMiniGameWon();

        if(CurrentFoodLeft == 0)
        {
            OnMiniGameLost();
            return;
        }

        canThrow = true;
    }

    private IEnumerator MoveGameObject(GameObject go, Vector3 pos, float overTime, Action callback)
    {
        float startTime = Time.time;
        Vector3 start = go.transform.position;

        while ((startTime + overTime) >= Time.time)
        {
            float t = (Time.time - startTime) / overTime;
            go.transform.position = Vector3.Lerp(start, pos, t);
            yield return null;
        }

        go.transform.position = pos;
        callback.Invoke();
    }

    private void SetLineRendererState(bool state)
    {
        ArcLineRenderer.enabled = state;
        ArcPointerGo.gameObject.SetActive(state);
    }

    private void UpdateLineRenderer(Vector3[] arc)
    {
        SetLineRendererState(true);

        ArcLineRenderer.positionCount = arc.Length;
        ArcLineRenderer.SetPositions(arc);
    }

    private Vector3[] GetThrowArc(Vector3 start, Vector3 end)
    {
        Vector3[] positions = new Vector3[ArcPointCount];
        Vector3 dir = end - start;

        for (int i = 0; i < positions.Length; i++)
        {
            float t = (float)((float)i / (float)positions.Length);
            positions[i] = start + (new Vector3(dir.x, 0, 0) * t) + (Vector3.up * ArcCurve.Evaluate(t) * ArcHeight);
        }

        return positions;
    }
}

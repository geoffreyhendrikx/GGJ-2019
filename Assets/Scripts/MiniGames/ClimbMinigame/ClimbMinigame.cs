﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ClimbMinigame : MonoBehaviour
{
    [SerializeField]
    private Canvas[] availableButtonCombinations;

    [SerializeField]
    private Animator thisAnimator;

    [SerializeField]
    private Image staminaBarFill, dotBarFill;

    [SerializeField]
    private UnityEvent onLosing, onWinning;

    [SerializeField]
    private int staminaBarChances = 3;
    private int currentStaminaBarChances = 3;

    [SerializeField]
    private Vector3 defaultLocalPositionOffset;

    [SerializeField]
    private float maxClimbHeight = 3, climbDuration = 1;

    [SerializeField]
    private Transform climbablesTransform;
    private ClimbableRock[] allClimbableRocks;

    [SerializeField]
    private Transform rightHand, leftHand;

    [SerializeField]
    private AudioSource climbingAudioSource;
    [SerializeField]
    private AudioClip[] climbingAudioClips;

    private ClimbableRock[] closestPoints = new ClimbableRock[4] { null, null, null, null };

    private int currentlyHangingOnRockId = -1;
    private ClimbableRock latestClimbingRock = null, previousClimbingRock = null;


    private void Awake()
    {
        allClimbableRocks = climbablesTransform.GetComponentsInChildren<ClimbableRock>();
        currentStaminaBarChances = staminaBarChances;

        for (int i = 0; i < allClimbableRocks.Length; i++)
            allClimbableRocks[i].SetupRock(i);

        FindClosestClimbableRocks();
        AssignButtonsToClimbableRocks();
    }

    private void Start()
    {
        InputManager.Instance.EnableInput();

        InputManager.Instance.OnAButtonPressedDown += CheckForClimb;
        InputManager.Instance.OnBButtonPressedDown += CheckForClimb;
    }

    private void OnDisable()
    {
        InputManager.Instance.OnAButtonPressedDown -= CheckForClimb;
        InputManager.Instance.OnBButtonPressedDown -= CheckForClimb;
    }

    /// <summary>
    /// Checks the button input (with exception of the directional input)
    /// </summary>
    private void CheckForClimb()
    {
        if (InputManager.Instance.DirectionalInputPressed == new Vector2Int(-1, 0))
            ClimbToPoint(InputManager.Instance.AButtonPressed ? 0 : 2);
        else if (InputManager.Instance.DirectionalInputPressed == new Vector2Int(1, 0))
            ClimbToPoint(InputManager.Instance.AButtonPressed ? 1 : 3);
    }

    /// <summary>
    /// Checks for the directional input. (with exception of the A/B buttons)
    /// </summary>
    /// <param name="value">Button input value</param>
    private void CheckForClimb(Vector2Int value)
    {
        CheckForClimb();
    }

    /// <summary>
    /// Climbs to a certain point.
    /// </summary>
    /// <param name="climbIndexNo">Climb index number</param>
    private void ClimbToPoint(int climbIndexNo)
    {
        if (closestPoints[climbIndexNo] == null)
            return;

        if (!latestClimbingRock || !latestClimbingRock.RockBroken)
            previousClimbingRock = latestClimbingRock;
        latestClimbingRock = closestPoints[climbIndexNo];
        climbingAudioSource.PlayOneShot(climbingAudioClips[Random.Range(0, climbingAudioClips.Length)]);
        StartCoroutine(ClimbToPointLerp(closestPoints[climbIndexNo]));
    }

    /// <summary>
    /// Triggers losing the game.
    /// </summary>
    private void LoseStamina()
    {
        latestClimbingRock.Break();
        float staminaBarChange = 1f / staminaBarChances;

        staminaBarFill.fillAmount -= staminaBarChange;
        dotBarFill.fillAmount += staminaBarChange;

        currentStaminaBarChances--;

        StopAllCoroutines();

        // Checking if the player is game over.
        if (currentStaminaBarChances == 0)
        {
            InputManager.Instance.DisableInput();

            onLosing.Invoke();
            climbingAudioSource.mute = true;
            StartCoroutine(RestartLevelDelayed());
        }
        else
        {
            StartCoroutine(ClimbToPointLerp(previousClimbingRock));

            FindClosestClimbableRocks();
            AssignButtonsToClimbableRocks();
        }
    }

    private IEnumerator RestartLevelDelayed()
    {
        yield return new WaitForSeconds(2);
        Game.Instance.RestartLevel();
    }

    private IEnumerator ClimbToPointLerp(ClimbableRock climbableRock)
    {
        currentlyHangingOnRockId = climbableRock.ID;
        InputManager.Instance.DisableInput();

        float tempValue = 0;
        Vector3 tempOffset = transform.position;

        if (climbableRock.transform.position.x > transform.position.x)
            tempOffset -= leftHand.transform.position;
        else
            tempOffset -= rightHand.transform.position;

        Vector3 fromPos = transform.position, toPos = climbableRock.transform.position + tempOffset;
        thisAnimator.SetBool("IsClimbing", true);

        while (tempValue < 1)
        {
            tempValue += Time.deltaTime / climbDuration;
            transform.position = Vector3.Lerp(fromPos, toPos, tempValue);

            yield return new WaitForEndOfFrame();

            // Checking if the rock is broken and react to it.
            if (tempValue >= 0.8f && climbableRock.RockBroken)
            {
                LoseStamina();
                break;
            }

            if (tempValue >= 1)
            {
                transform.position = toPos;

                thisAnimator.SetBool("IsClimbing", false);

                FindClosestClimbableRocks();
                AssignButtonsToClimbableRocks();
                InputManager.Instance.EnableInput();
            }
        }
    }

    /// <summary>
    /// Will find the closest climbable rocks and updates the closest points based on them.
    /// </summary>
    private void FindClosestClimbableRocks()
    {
        // First empty everything.
        for (int i = 0; i < closestPoints.Length; i++)
            closestPoints[i] = null;

        for (int i = 0; i < allClimbableRocks.Length; i++)
        {
            if (currentlyHangingOnRockId == i || allClimbableRocks[i].BrokenRockRevealed || Vector3.Distance(leftHand.position, allClimbableRocks[i].transform.position) > maxClimbHeight || allClimbableRocks[i].transform.position.y < leftHand.position.y)
                continue;

            for (int j = 0; j < closestPoints.Length; j++)
            {
                // Let's check if the right or left hand are close to the climbables.
                if (closestPoints[j] == null ||
                    Vector3.Distance(allClimbableRocks[i].transform.position, transform.position) < Vector3.Distance(closestPoints[j].transform.position, transform.position))
                {
                    bool tempDuplicatedValue = false;

                    // Checking if there is not a duplicated point.
                    for (int k = 0; k < closestPoints.Length; k++)
                    {
                        if (closestPoints[k] == allClimbableRocks[i])
                        {
                            tempDuplicatedValue = true;
                            break;
                        }
                    }

                    if (tempDuplicatedValue)
                        continue;

                    if (j < closestPoints.Length - 1)
                        closestPoints[j + 1] = closestPoints[j];

                    closestPoints[j] = allClimbableRocks[i];
                }
            }
        }

        // Nothing above the player anymore? YOU WON!
        if (closestPoints[0] == null)
        {
            onWinning.Invoke();
            Game.Instance.NextScene();
        }
    }

    /// <summary>
    /// Assigns buttons to the climbable rocks.
    /// </summary>
    private void AssignButtonsToClimbableRocks()
    {
        for (int i = 0; i < closestPoints.Length; i++)
        {
            if (closestPoints[i] == null)
            {
                availableButtonCombinations[i].gameObject.SetActive(false);
                continue;
            }

            availableButtonCombinations[i].gameObject.SetActive(true);
            availableButtonCombinations[i].transform.SetParent(closestPoints[i].transform);
            availableButtonCombinations[i].transform.localPosition = defaultLocalPositionOffset;
        }
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class LerpCameraAction : MonoBehaviour
{
    public float LerpTime;
    public Vector3 DesiredPosition;
    public UnityEvent OnLerpDone;

    public void LerpToPosition()
    {
        CameraHandler.Instance.enabled = false;
        StartCoroutine(DoLerp());
    }

    private IEnumerator DoLerp()
    {
        float startTime = Time.time;
        Vector3 startPosition = CameraHandler.Instance.transform.position;

        while((startTime + LerpTime) >= Time.time)
        {
            float t = (Time.time - startTime) / LerpTime;
            CameraHandler.Instance.transform.position = Vector3.Lerp(startPosition,DesiredPosition,t);
            yield return null;
        }

        CameraHandler.Instance.transform.position = DesiredPosition;
        OnLerpDone.Invoke();
    }
}
